//Package models has the model base in it
package models

import (
	"errors"

	rethink "github.com/GoRethink/gorethink"
)

//ModelInterface is the interace for generic models
type ModelInterface interface {
	Increment()
	UpdateArgs() (rethink.InsertOpts, error)
	OnConflict() func(rethink.Term, rethink.Term, rethink.Term) interface{}
	GetID() ID
	GetVersion() Version
	GetCandidateVersion() Version
	GetLock() Lock
	ShiftVersions()
	SameModel(ModelInterface) bool
	SameVersion(ModelInterface) bool
	GetGroups() IDs
	GetReaders() IDs
	GetWriters() IDs
	SetGroup(ID)
	SetReader(ID)
	SetWriter(ID)
}

//IDs is a list of IDs
type IDs []ID

//Add an id to the slice if it isn't already there
func (lst IDs) Add(nid ID) {
	var has bool
	for _, oid := range lst {
		has = oid.Equals(nid)
		if has {
			break
		}
	}
	if !has {
		lst = append(lst, nid)
	}
}

//Remove an ID from the list if it's there
func (lst IDs) Remove(nid ID) {
	var has bool
	var oid ID
	var ix int
	for ix, oid = range lst {
		has = oid.Equals(nid)
		if has {
			break
		}
	}
	if has && ix == len(lst)-1 {
		lst = lst[:ix]
	} else if has {
		lst = append(lst[:ix], lst[ix+1:]...)
	}
}

//Has determines if an ID is in the list
func (lst IDs) Has(nid ID) bool {
	var has bool
	for _, oid := range lst {
		has = oid.Equals(nid)
		if has {
			break
		}
	}
	return has
}

//Model is out baseline model type
type Model struct {
	ID               ID      `json:"id"`
	Version          Version `json:"version"`
	CandidateVersion Version `json:"candidateVersion,omitempty"`
	Hidden           bool    `json:"hidden"`
	Lock             Lock    `json:"lock"`
	Groups           IDs     `json:"groups,omitempty"`
	Readers          IDs     `json:"readers,omitempty"`
	Writers          IDs     `json:"writers,omitempty"`
}

//SameModel lets you know if the models are the same
func (mdl *Model) SameModel(omdl ModelInterface) bool {
	return mdl.ID.Equals(omdl.GetID())
}

//SameVersion lets you know if the models are the same and same version
func (mdl *Model) SameVersion(omdl ModelInterface) bool {
	//Do not need to both so if this service ever scales just check the version
	return mdl.SameModel(omdl) && mdl.Version.Equals(omdl.GetVersion())
}

//GetID returns the ID
func (mdl *Model) GetID() ID {
	return mdl.ID
}

//GetVersion returns the Version
func (mdl *Model) GetVersion() Version {
	return mdl.Version
}

//GetCandidateVersion returns the CandidateVersion
func (mdl *Model) GetCandidateVersion() Version {
	return mdl.CandidateVersion
}

//GetLock returns the Lock
func (mdl *Model) GetLock() Lock {
	return mdl.Lock
}

//GetGroups returns the groups
func (mdl *Model) GetGroups() IDs {
	return mdl.Groups
}

//GetReaders returns the readers
func (mdl *Model) GetReaders() IDs {
	return mdl.Readers
}

//GetWriters returns the writers
func (mdl *Model) GetWriters() IDs {
	return mdl.Writers
}

//SetGroup returns the groups
func (mdl *Model) SetGroup(id ID) {
	mdl.Groups = append(mdl.Groups, id)
}

//SetReader returns the readers
func (mdl *Model) SetReader(id ID) {
	mdl.Readers = append(mdl.Readers, id)
}

//SetWriter returns the writers
func (mdl *Model) SetWriter(id ID) {
	mdl.Writers = append(mdl.Writers, id)
}

//ShiftVersions replaces the version with the candidate version and nulls the candidate
func (mdl *Model) ShiftVersions() {
	mdl.Version = mdl.CandidateVersion
	mdl.CandidateVersion = Version{}
}

//Increment increments a model to the next version creating an id if required
func (mdl *Model) Increment() {
	if mdl.ID.IsZero() {
		mdl.ID.Init()
	} else {
		mdl.CandidateVersion.Init()
	}
	return
}

//OnConflict is the conflict function for a model on insert
func (mdl *Model) OnConflict() func(rethink.Term, rethink.Term, rethink.Term) interface{} {
	return func(id, oldDoc, newDoc rethink.Term) interface{} {
		return rethink.Branch(
			oldDoc.Field("version").Eq(newDoc.Field("version")),
			newDoc.Merge(rethink.Object("version", newDoc.Field("candidateVersion"))).Without("candidateVersion"),
			oldDoc,
		)
	}
}

//UpdateArgs returns parameters to update the model in rethink
func (mdl *Model) UpdateArgs() (rethink.InsertOpts, error) {
	var err error
	if mdl.CandidateVersion.IsZero() {
		err = errors.New("Model hasn't been incremented")
	}
	return rethink.InsertOpts{
		Conflict:      mdl.OnConflict(),
		ReturnChanges: true,
	}, err
}
