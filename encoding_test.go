package models

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUID(t *testing.T) {
	Convey("Given a new UID", t, func() {
		id := UID{}
		So(id, ShouldBeZeroValue)
		Convey("the id should instantiate just once", func() {
			So(id, ShouldEqual, UID{})
			id.Init()
			So(id, ShouldNotEqual, UID{})
			s := id.ToString()
			b := id.ToBytes()
			b64 := id.ToBase64()
			id.Init()
			So(s, ShouldEqual, id.ToString())
			So(b, ShouldResemble, id.ToBytes())
			So(b64, ShouldResemble, id.ToBase64())
		})
	})
}

func TestUIDMarshal(t *testing.T) {
	Convey("Given a new UID", t, func() {
		id := UID{}
		id.Init()
		Convey("the id should instantiate just once", func() {
			d, e := json.Marshal(id)
			So(e, ShouldBeNil)
			nid := UID{}
			e = json.Unmarshal(d, &nid)
			So(e, ShouldBeNil)
			So(nid, ShouldEqual, id)
		})
	})
}

func TestIDMarshal(t *testing.T) {
	Convey("Given a new UID", t, func() {
		id := ID{}
		id.Init()
		Convey("the id should instantiate just once", func() {
			d, e := json.Marshal(id)
			So(e, ShouldBeNil)
			nid := ID{}
			e = json.Unmarshal(d, &nid)
			So(e, ShouldBeNil)
			So(nid.Equals(id), ShouldBeTrue)
		})
	})
}

func TestID(t *testing.T) {
	Convey("Given a new ID", t, func() {
		id := ID{}
		So(id, ShouldBeZeroValue)
		Convey("the id should instantiate just once", func() {
			So(id.Equals(ID{}), ShouldBeTrue)
			id.Init()
			So(id.Equals(ID{}), ShouldBeFalse)
			s := id.ToString()
			b := id.ToBytes()
			b64 := id.ToBase64()
			id.Init()
			So(s, ShouldEqual, id.ToString())
			So(b, ShouldResemble, id.ToBytes())
			So(b64, ShouldResemble, id.ToBase64())
		})
	})
}

func TestModel(t *testing.T) {
	Convey("Given a blank model", t, func() {
		Convey("We should be able to marshall and unmarshall it", func() {
			mdl := Model{}
			b, e := json.Marshal(mdl)
			So(e, ShouldBeNil)
			So(b, ShouldNotBeEmpty)
			e = json.Unmarshal(b, &mdl)
			So(e, ShouldBeNil)
		})
	})
}
