package models

import (
	"log"
	"testing"

	rethink "github.com/GoRethink/gorethink"

	. "github.com/smartystreets/goconvey/convey"
)

func buildExpectation(mock *rethink.Mock, mdl *Model) {
	ua, _ := mdl.UpdateArgs()
	mock.On(rethink.Table("models").Insert(mdl, ua)).Return(
		map[string]interface{}{
			"version": mdl.CandidateVersion.ToBase64(),
			"id":      mdl.ID.ToBase64(),
		},
		nil,
	)
}

func TestModelInsert(t *testing.T) {
	rethink.SetTags("gorethink", "json")
	mock := rethink.NewMock()
	Convey("Given a new model", t, func() {
		mdl := &Model{}
		mdl.Increment()
		buildExpectation(mock, mdl)
		Convey("the model should write to mock db", func() {
			ua, _ := mdl.UpdateArgs()
			res, err := rethink.Table("models").Insert(mdl, ua).Run(mock)
			log.Fatal(res)
			So(err, ShouldBeNil)
			nmdl := []byte{}
			err = res.One(nmdl)
			log.Fatal(nmdl)
		})
	})
}
