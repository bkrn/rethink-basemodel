//Package models has the model base in it
package models

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"

	"github.com/satori/go.uuid"
)

const idLength int = 16

type UID [idLength]byte

//Init populates a new UID
func (id *UID) Init() {
	for _, b := range id {
		if b != 0 {
			return
		}
	}
	for ix, b := range uuid.NewV4().Bytes() {
		id[ix] = b
	}
}

//ToBytes converts a UID to a byte slice
func (id *UID) ToBytes() []byte {
	bs := make([]byte, idLength)
	for ix, b := range id {
		bs[ix] = b
	}
	return bs
}

//FromBytes converts a UID from a byte slice
func (id *UID) FromBytes(src []byte) error {
	if len(src) != len(id) {
		return errors.New("bytes have wrong length for UID conversion")
	}
	for ix, v := range src {
		id[ix] = v
	}
	return nil
}

//ToBase64 converts to base64 bytes
func (id *UID) ToBase64() []byte {
	dst := make([]byte, base64.RawURLEncoding.EncodedLen(idLength))
	buf := bytes.NewBuffer([]byte{})
	buf.WriteByte(34)
	base64.RawURLEncoding.Encode(dst, id.ToBytes())
	buf.Write(dst)
	buf.WriteByte(34)
	return buf.Bytes()
}

//ToString converts a UID to a base64 encoded string
func (id *UID) ToString() string {
	b64 := id.ToBase64()
	return string(b64[1 : len(b64)-1])
}

//FromString converts to UID from a base64 encoded string
func (id *UID) FromString(s string) {
	s = fmt.Sprintf("'%s'", s)
	id.FromBase64([]byte(s))
}

//FromBase64 converts a base64 bytes to a UID
func (id *UID) FromBase64(src []byte) error {
	var strippedSource []byte
	if len(src) > 1 {
		strippedSource = src[1 : len(src)-1]
	}
	if base64.RawURLEncoding.DecodedLen(len(strippedSource)) != idLength {
		return fmt.Errorf(
			"base64 encoded UID has wrong decoded length: %d instead of %d",
			base64.RawURLEncoding.DecodedLen(len(strippedSource)),
			idLength,
		)
	}
	dst := make([]byte, idLength)
	_, err := base64.RawURLEncoding.Decode(dst, strippedSource)
	if err != nil {
		return err
	}
	return id.FromBytes(dst)
}

//MarshalJSON marshalls the UID
func (id UID) MarshalJSON() ([]byte, error) {
	return id.ToBase64(), nil
}

//UnmarshalJSON unmarshals the UID
func (id *UID) UnmarshalJSON(data []byte) error {
	return id.FromBase64(data)
}

//IsZero tests if a UID is its zero value
func (id UID) IsZero() (zero bool) {
	for _, b := range id {
		if zero = b == 0; !zero {
			return zero
		}
	}
	return
}

//ID is out baseline ID type
type ID struct{ UID }

func (id ID) MarshalRQL() (interface{}, error) {
	return id.ToBase64(), nil
}

func (id *ID) UnmarshalRQL(data interface{}) error {
	var d []byte
	var t bool
	if d, t = data.([]byte); !t {
		return errors.New("Wrong type for rql uid")
	}
	return id.FromBase64(d)
}

//MarshalJSON marshalls the UID
func (id ID) MarshalJSON() ([]byte, error) {
	return id.ToBase64(), nil
}

//UnmarshalJSON unmarshals the UID
func (id *ID) UnmarshalJSON(data []byte) error {
	return id.FromBase64(data)
}

//Get returns the id at an index
func (id *ID) Get(ix int) byte {
	return id.UID[ix]
}

//Equals tests if two UIDs are equal
func (id *ID) Equals(other ID) (equal bool) {
	for ix, v := range id.UID {
		if equal = v == other.Get(ix); !equal {
			return
		}
	}
	return
}

//Version is the version of a model
type Version struct{ UID }

func (id Version) MarshalRQL() (interface{}, error) {
	return id.ToBase64(), nil
}

func (id *Version) UnmarshalRQL(data interface{}) error {
	var d []byte
	var t bool
	if d, t = data.([]byte); !t {
		return errors.New("Wrong type for rql uid")
	}
	return id.FromBase64(d)
}

//MarshalJSON marshalls the UID
func (id Version) MarshalJSON() ([]byte, error) {
	return id.ToBase64(), nil
}

//UnmarshalJSON unmarshals the UID
func (id *Version) UnmarshalJSON(data []byte) error {
	return id.FromBase64(data)
}

//Get returns the id at an index
func (id *Version) Get(ix int) byte {
	return id.UID[ix]
}

//Equals tests if two UIDs are equal
func (id *Version) Equals(other Version) (equal bool) {
	for ix, v := range id.UID {
		if equal = v == other.Get(ix); !equal {
			return
		}
	}
	return
}

//Lock is a secret to lock writes to a model
type Lock struct{ UID }

func (id Lock) MarshalRQL() (interface{}, error) {
	return id.ToBase64(), nil
}

func (id *Lock) UnmarshalRQL(data interface{}) error {
	var d []byte
	var t bool
	if d, t = data.([]byte); !t {
		return errors.New("Wrong type for rql uid")
	}
	return id.FromBase64(d)
}

//MarshalJSON marshalls the UID
func (id Lock) MarshalJSON() ([]byte, error) {
	return id.ToBase64(), nil
}

//UnmarshalJSON unmarshals the UID
func (id *Lock) UnmarshalJSON(data []byte) error {
	return id.FromBase64(data)
}

//Get returns the id at an index
func (id *Lock) Get(ix int) byte {
	return id.UID[ix]
}

//Equals tests if two UIDs are equal
func (id *Lock) Equals(other Lock) (equal bool) {
	for ix, v := range id.UID {
		if equal = v == other.Get(ix); !equal {
			return
		}
	}
	return
}
